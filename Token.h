// Clase Token

#include <iostream>

using namespace std;

class Token {
    private:
        string value;
        int priority = 0;
    public:
        Token() {}

        // Constructor. a partir del valor de determina la prioridad.
        Token(string value) {
            this->value = value;
            if (!isNum()) {
                if (!value.compare("+") || !value.compare("-")) {
                    setPriority(1);
                } else if (!value.compare("/") || !value.compare("*")) {
                    setPriority(2);
                } else if (!value.compare("(") || !value.compare(")")){
                    setPriority(3);
                }else{
                    setPriority(4);
                }
            }
        }

        // Seter y getter del valor del nodo
        void setValue(string v) {
            value = v;
        }

        string getValue() {
            return value;
        }

        // Evalua si el token es numerico o no
        bool isNum() {
            for (int x = 0; x < value.length(); x++) {
                if (!isdigit(value[x]))
                    return false;
            }
            return true;
        }

        // Setter y getter prioridad
        void setPriority(int x) {
            priority = x;
        }

        int getPriority() {
            return priority;
        }
};
